package com.zebrunner.iam.domain.search;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchResult<T> {

    private String query;
    private String orderBy;
    private SortOrder sortOrder;
    private Integer page;
    private Integer pageSize;

    private long totalPages;
    private long totalResults;
    private List<T> results;

    public SearchResult(SearchCriteria searchCriteria, long totalPages, long totalResults, List<T> results) {
        this.query = searchCriteria.getQuery();
        this.orderBy = searchCriteria.getSortBy();
        this.sortOrder = searchCriteria.getSortOrder();
        this.page = searchCriteria.getPage();
        this.pageSize = searchCriteria.getPageSize();

        this.totalPages = totalPages;
        this.totalResults = totalResults;
        this.results = results;
    }

    public <R> SearchResult(SearchResult<R> searchResult, List<T> results) {
        this.query = searchResult.getQuery();
        this.orderBy = searchResult.getOrderBy();
        this.sortOrder = searchResult.getSortOrder();
        this.page = searchResult.getPage();
        this.pageSize = searchResult.getPageSize();

        this.totalPages = searchResult.getTotalPages();
        this.totalResults = searchResult.getTotalResults();
        this.results = results;
    }

}
