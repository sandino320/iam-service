package com.zebrunner.iam.domain.entity;

import java.util.Arrays;

public enum UserStatus {

    ACTIVE,
    INACTIVE;

    public static UserStatus fromString(String value) {
        return Arrays.stream(UserStatus.values())
                     .filter(status -> status.name().equals(value))
                     .findFirst()
                     .orElse(null);
    }

}
