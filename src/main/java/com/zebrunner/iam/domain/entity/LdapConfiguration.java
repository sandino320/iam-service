package com.zebrunner.iam.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ldap_configuration")
public class LdapConfiguration implements Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private boolean enabled;
    private String url;
    private String managerUser;
    private String managerPassword;
    private String dn;
    private String searchFilter;
    private String passwordKey;

    @Override
    @SneakyThrows
    public LdapConfiguration clone() {
        return (LdapConfiguration) super.clone();
    }
}
