package com.zebrunner.iam.web.documentation.v1;

import com.zebrunner.iam.web.request.v1.LoginRequest;
import com.zebrunner.iam.web.request.v1.VerifyPermissionsRequest;
import com.zebrunner.iam.web.request.v1.RefreshAuthTokenRequest;
import com.zebrunner.iam.web.response.v1.AuthenticationDataResponse;
import com.zebrunner.iam.web.security.AuthenticatedUser;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface AuthenticationDocumentedController {

    @Operation(
            summary = "Generates an authentication data.",
            description = "Returns the generated auth and refresh tokens that might be used to authenticate on API calls.",
            tags = "Authentication",
            requestBody = @RequestBody(
                    description = "Credentials for user authentication",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = LoginRequest.class)
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            headers = @Header(name = "x-zbr-first-login", description = "Returned only on first login."),
                            description = "Returns the authentication data including auth and refresh tokens, user id and expiration for auth token.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = AuthenticationDataResponse.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "Indicates that the user credentials are invalid.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    ResponseEntity<AuthenticationDataResponse> login(LoginRequest loginRequest);

    @Operation(
            summary = "Refreshes an auth token.",
            description = "Returns a new authentication data..",
            tags = "Authentication",
            parameters = @Parameter(name = "refreshToken", description = "Refresh token that was issued on login or refresh operation"),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Returns a new authentication data.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = AuthenticationDataResponse.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "Indicates that the provided refresh token is expired or user password has been changed since the token was issued.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "Indicates that the corresponding user is in INACTIVE state.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Indicates that the corresponding user does not exist.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    AuthenticationDataResponse refresh(RefreshAuthTokenRequest refreshAuthTokenRequest);

    @Operation(
            summary = "Generates an API access token.",
            description = "Returns the token that provides access to API. Technically this is a refresh token. /refresh endpoint should be invoked to obtain an access token.",
            tags = "Authentication",
            parameters = @Parameter(name = HttpHeaders.AUTHORIZATION, in = ParameterIn.HEADER, description = "The auth token (Bearer)"),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Returns the generated API access token.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = Map.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "Indicates that the user attempting to generate an api access token is not authenticated.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Indicates that the user attempting to generate an api access token does not exist.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    Map<String, String> getServiceRefreshToken(AuthenticatedUser authenticatedUser);

    @Operation(
            tags = "Authentication"
    )
    ResponseEntity<Void> checkPermissions(VerifyPermissionsRequest verifyPermissionsRequest);

}
