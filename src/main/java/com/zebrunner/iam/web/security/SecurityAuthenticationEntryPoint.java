package com.zebrunner.iam.web.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zebrunner.iam.exception.CommonErrorDetails;
import com.zebrunner.iam.web.error.ErrorResponse;
import com.zebrunner.iam.web.error.ErrorResponseHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

/**
 * SecurityAuthenticationEntryPoint is called by ExceptionTranslationFilter to handle all AuthenticationException. These
 * exceptions are thrown when authentication failed : wrong login/password, authentication unavailable, invalid token
 * authentication expired, etc.
 * <p>
 * For problems related to access (roles), see RestAccessDeniedHandler.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class SecurityAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private final ErrorResponseHelper errorResponseHelper;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        ErrorResponse errorResponse = errorResponseHelper.buildFromDetail(CommonErrorDetails.UNAUTHORIZED);
        log.debug(errorResponse.getMessage(), authException);

        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(response);
        wrapper.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        wrapper.setContentType(MediaType.APPLICATION_JSON_VALUE);
        wrapper.getWriter().println(new ObjectMapper().writeValueAsString(errorResponse));
        wrapper.getWriter().flush();
    }
}