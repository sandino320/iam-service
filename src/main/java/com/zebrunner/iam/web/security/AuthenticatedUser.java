package com.zebrunner.iam.web.security;

import com.zebrunner.iam.domain.entity.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class AuthenticatedUser implements UserDetails {

    private Integer userId;
    private String username;
    private String password;
    private UserStatus status;
    private Set<String> permissionsSuperset;

    public AuthenticatedUser(Integer userId, String username, Set<String> permissionsSuperset) {
        this.userId = userId;
        this.username = username;
        this.permissionsSuperset = permissionsSuperset;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return permissionsSuperset.stream()
                                  .map(permission -> (GrantedAuthority) () -> permission)
                                  .collect(Collectors.toSet());
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return UserStatus.ACTIVE.equals(status);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return UserStatus.ACTIVE.equals(status);
    }
}
