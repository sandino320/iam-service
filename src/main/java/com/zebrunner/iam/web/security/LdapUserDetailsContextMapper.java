package com.zebrunner.iam.web.security;

import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.service.PermissionService;
import com.zebrunner.iam.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;

@Slf4j
@Component
@RequiredArgsConstructor
public class LdapUserDetailsContextMapper implements UserDetailsContextMapper {

    private final UserService userService;
    private final PermissionService permissionService;

    @Override
    public UserDetails mapUserFromContext(DirContextOperations operations,
                                          String username,
                                          Collection<? extends GrantedAuthority> authorities) {
        User user = userService.getByUsernameOrEmail(username)
                               .orElseThrow(() -> new UsernameNotFoundException("User with username " + username + " was not found"));
        Set<String> permissions = permissionService.getSuperset(user.getGroups(), user.getPermissions());
        return new AuthenticatedUser(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getStatus(),
                permissions
        );
    }

    @Override
    public void mapUserToContext(UserDetails user, DirContextAdapter adapter) {
        // Do nothing
    }
}
