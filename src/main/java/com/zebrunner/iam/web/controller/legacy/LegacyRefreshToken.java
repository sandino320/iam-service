package com.zebrunner.iam.web.controller.legacy;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LegacyRefreshToken {

    private String type;
    private String accessToken;
    private String tenant;

}
