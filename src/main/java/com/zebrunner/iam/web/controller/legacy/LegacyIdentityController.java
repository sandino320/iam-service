package com.zebrunner.iam.web.controller.legacy;

import com.zebrunner.iam.domain.token.AuthenticationData;
import com.zebrunner.iam.exception.EntityNotFoundException;
import com.zebrunner.iam.service.AuthenticationService;
import com.zebrunner.iam.service.UserService;
import com.zebrunner.iam.service.exception.EntityNotFoundError;
import com.zebrunner.iam.web.request.v1.RefreshAuthTokenRequest;
import com.zebrunner.iam.web.security.AuthenticatedUser;
import com.zebrunner.iam.web.security.Principal;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Deprecated
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class LegacyIdentityController {

    private final AuthenticationService authenticationService;
    private final UserService userService;

    @GetMapping("/users/profile")
    public LegacyUserInfo getUserInfoByToken(@Principal AuthenticatedUser authenticatedUser) {
        return LegacyUserInfo.builder()
                             .id(Long.valueOf(authenticatedUser.getUserId()))
                             .username(authenticatedUser.getUsername())
                             .build();
    }

    @GetMapping(path = "/users/profile", params = "username")
    public LegacyUserInfo getUserInfoByUsername(@RequestParam("username") String username) {
        return userService.getWithPermissionsByUsername(username)
                          .map(user -> LegacyUserInfo.builder().id(Long.valueOf(user.getId()))
                                                     .username(user.getUsername()).build())
                          .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.USER_NOT_FOUND_BY_USERNAME));
    }

    @PostMapping(path = "/auth/refresh")
    public LegacyRefreshToken refreshToken(@RequestBody RefreshAuthTokenRequest request) {
        String refreshToken = request.getRefreshToken();
        AuthenticationData authenticationData = authenticationService.refresh(refreshToken);

        return LegacyRefreshToken.builder()
                                 .type(authenticationData.getAuthTokenType())
                                 .accessToken(authenticationData.getAuthToken())
                                 .tenant("zafira")
                                 .build();
    }

}
