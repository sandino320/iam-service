package com.zebrunner.iam.web.request.v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.googlecode.jmapper.annotations.JMapConversion;
import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.entity.Source;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@JGlobalMap
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SaveInvitationRequest {

    @NotBlank
    private String email;
    @NotNull
    @Positive
    private Integer groupId;
    @NotNull
    private Source source;

    @JMapConversion(from = {"groupId"}, to = {"groupId"})
    public Integer convertGroupId(Integer groupId) {
        return groupId;
    }

    @JMapConversion(from = {"groupId"}, to = {"group"})
    public Group convertGroup(Integer groupId) {
        return Group.builder().id(groupId).build();
    }

}
