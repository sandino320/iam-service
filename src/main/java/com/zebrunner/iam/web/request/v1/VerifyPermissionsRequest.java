package com.zebrunner.iam.web.request.v1;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
public class VerifyPermissionsRequest {

    @NotEmpty
    private String authToken;
    @NotEmpty
    private Set<String> permissions;

}
