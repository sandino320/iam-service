package com.zebrunner.iam.web.request.v1;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ResetPasswordRequest {

    @NotEmpty
    private String resetToken;
    @NotEmpty
    private String newPassword;

}
