package com.zebrunner.iam.web.request.v1;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.googlecode.jmapper.annotations.JMapConversion;
import com.zebrunner.iam.domain.entity.Permission;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@JGlobalMap
public class SaveGroupRequest {

    @NotBlank
    private String name;
    @JsonProperty("isDefault")
    private Boolean isDefault = false;
    private Boolean invitable = true;
    private List<String> permissions = List.of();



    @JMapConversion(from = {"permissions"}, to = {"permissions"})
    public Set<Permission> convertPermissions(List<String> permissions) {
        return permissions.stream()
                          .map(permission -> new Permission(null, permission))
                          .collect(Collectors.toSet());
    }

}
