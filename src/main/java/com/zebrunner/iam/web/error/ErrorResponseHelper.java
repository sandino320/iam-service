package com.zebrunner.iam.web.error;

import com.zebrunner.iam.exception.ErrorDetail;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ErrorResponseHelper {

    private final String serviceCode;
    private final MessageSource messageSource;

    public ErrorResponseHelper(@Value("${service.exception.service-domain:APP}") String serviceCode,
                               MessageSource messageSource) {
        this.serviceCode = serviceCode;
        this.messageSource = messageSource;
    }

    public ErrorResponse buildFromDetail(ErrorDetail errorDetail, Object... args) {
        String errorCode = errorCode(errorDetail.getCode());
        String localizedMessage = getLocalizedMessage(errorDetail.getCode(), args);

        return new ErrorResponse(errorCode, localizedMessage);
    }

    public ErrorResponse buildFromDetail(ErrorDetail errorDetail, List<FieldError> fieldErrors) {
        String errorCode = errorCode(errorDetail.getCode());
        String localizedMessage = getLocalizedMessage(errorDetail.getCode());

        return new ErrorResponse(errorCode, localizedMessage, fieldErrors);
    }

    private String errorCode(Integer numericCode) {
        return serviceCode + '-' + numericCode;
    }

    private String getLocalizedMessage(Integer numericCode, Object... args) {
        String messageKey = String.format("error.%d.message", numericCode);
        return messageSource.getMessage(messageKey, args, LocaleContextHolder.getLocale());
    }

}
