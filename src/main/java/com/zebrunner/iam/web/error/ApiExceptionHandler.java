package com.zebrunner.iam.web.error;

import com.zebrunner.iam.exception.AuthException;
import com.zebrunner.iam.exception.BusinessValidationException;
import com.zebrunner.iam.exception.CommonErrorDetails;
import com.zebrunner.iam.exception.EntityNotFoundException;
import com.zebrunner.iam.exception.ExternalSystemException;
import com.zebrunner.iam.exception.ForbiddenOperationException;
import com.zebrunner.iam.exception.TooManyOperationsException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class ApiExceptionHandler {

    private final ErrorResponseHelper errorResponseHelper;

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BusinessValidationException.class)
    public ErrorResponse handleBusinessValidationException(BusinessValidationException e) {
        ErrorResponse errorResponse = errorResponseHelper.buildFromDetail(e.getErrorDetail(), e.getMessageArgs());
        log.debug(errorResponse.getMessage(), e);
        return errorResponse;
    }

    @ExceptionHandler(AuthException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorResponse handleAuthException(AuthException e) {
        ErrorResponse errorResponse = errorResponseHelper.buildFromDetail(e.getErrorDetail(), e.getMessageArgs());
        log.debug(errorResponse.getMessage(), e);
        return errorResponse;
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(ForbiddenOperationException.class)
    public ErrorResponse handleForbiddenOperationException(ForbiddenOperationException e) {
        ErrorResponse errorResponse = errorResponseHelper.buildFromDetail(e.getErrorDetail(), e.getMessageArgs());
        log.debug(errorResponse.getMessage(), e);
        return errorResponse;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    public ErrorResponse handleEntityNotFoundException(EntityNotFoundException e) {
        ErrorResponse errorResponse = errorResponseHelper.buildFromDetail(e.getErrorDetail(), e.getMessageArgs());
        log.debug(errorResponse.getMessage(), e);
        return errorResponse;
    }

    @ResponseStatus(HttpStatus.TOO_MANY_REQUESTS)
    @ExceptionHandler(TooManyOperationsException.class)
    public ErrorResponse handleTooManyOperationsException(TooManyOperationsException e) {
        ErrorResponse errorResponse = errorResponseHelper.buildFromDetail(e.getErrorDetail(), e.getMessageArgs());
        log.debug(errorResponse.getMessage(), e);
        return errorResponse;
    }

    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler(ExternalSystemException.class)
    public ErrorResponse handleExternalSystemException(ExternalSystemException e) {
        ErrorResponse errorResponse = errorResponseHelper.buildFromDetail(e.getErrorDetail(), e.getMessageArgs());
        log.debug(errorResponse.getMessage(), e);
        return errorResponse;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        ErrorResponse errorResponse = buildErrorResponse(e.getBindingResult());
        log.error("Method argument not valid error (related to @Valid annotation): " + errorResponse.getMessage(), e);
        return errorResponse;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BindException.class)
    public ErrorResponse handleBindException(BindException e) {
        ErrorResponse errorResponse = buildErrorResponse(e.getBindingResult());
        log.error("Binding error (related to @Valid annotation): " + errorResponse.getMessage(), e);
        return errorResponse;
    }

    private ErrorResponse buildErrorResponse(BindingResult bindingResult) {
        List<FieldError> fieldErrors = Optional.ofNullable(bindingResult)
                                               .map(BindingResult::getFieldErrors)
                                               .orElseGet(List::of)
                                               .stream()
                                               .map(this::toFieldError)
                                               .collect(Collectors.toList());

        return errorResponseHelper.buildFromDetail(CommonErrorDetails.VALIDATION_ERROR, fieldErrors);
    }

    private FieldError toFieldError(org.springframework.validation.FieldError fieldError) {
        return new FieldError(fieldError.getField(), fieldError.getDefaultMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public ErrorResponse handleConstraintViolationException(ConstraintViolationException e) {
        List<FieldError> fieldErrors = e.getConstraintViolations()
                                        .stream()
                                        .map(this::toFieldError)
                                        .collect(Collectors.toList());
        ErrorResponse errorResponse = errorResponseHelper
                .buildFromDetail(CommonErrorDetails.VALIDATION_ERROR, fieldErrors);
        log.error("Constraint violation error (related to @Validated annotation): " + errorResponse.getMessage(), e);
        return errorResponse;
    }

    private FieldError toFieldError(ConstraintViolation<?> constraintViolation) {
        String field = StreamSupport.stream(constraintViolation.getPropertyPath().spliterator(), false)
                                    .reduce((node, node2) -> node2)
                                    .map(Path.Node::getName)
                                    .orElse(null);
        return new FieldError(field, constraintViolation.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ErrorResponse handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        ErrorResponse errorResponse = errorResponseHelper.buildFromDetail(CommonErrorDetails.HTTP_MESSAGE_NOT_READABLE);
        log.debug("Can not convert payload message: " + errorResponse.getMessage(), e);
        return errorResponse;
    }

    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ErrorResponse handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        ErrorResponse errorResponse = errorResponseHelper
                .buildFromDetail(CommonErrorDetails.HTTP_METHOD_NOT_SUPPORTED, e.getMethod());
        log.debug(errorResponse.getMessage(), e);
        return errorResponse;
    }

    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ErrorResponse handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException e) {
        MediaType contentType = e.getContentType();
        ErrorResponse errorResponse = errorResponseHelper
                .buildFromDetail(CommonErrorDetails.HTTP_MEDIA_TYPE_NOT_SUPPORTED, contentType);
        log.debug(errorResponse.getMessage(), e);
        return errorResponse;
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ErrorResponse handleAccessDeniedException(AccessDeniedException e) {
        ErrorResponse errorResponse = errorResponseHelper.buildFromDetail(CommonErrorDetails.ACCESS_IS_DENIED);
        log.error("Access is not authorized: " + errorResponse.getMessage(), e);
        return errorResponse;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleUnexpectedException(Exception e) {
        ErrorResponse errorResponse = errorResponseHelper.buildFromDetail(CommonErrorDetails.INTERNAL_ERROR);
        log.error("Unexpected internal server error: " + errorResponse.getMessage(), e);
        return errorResponse;
    }

}
