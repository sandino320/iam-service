package com.zebrunner.iam.service.impl;

import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.entity.Invitation;
import com.zebrunner.iam.domain.entity.Source;
import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.domain.search.SearchCriteria;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.exception.ApplicationException;
import com.zebrunner.iam.exception.BusinessValidationException;
import com.zebrunner.iam.exception.EntityNotFoundException;
import com.zebrunner.iam.messaging.sender.EmailSender;
import com.zebrunner.iam.persistance.InvitationRepository;
import com.zebrunner.iam.service.GroupService;
import com.zebrunner.iam.service.InvitationService;
import com.zebrunner.iam.service.config.OwnerProperties;
import com.zebrunner.iam.service.exception.BusinessValidationError;
import com.zebrunner.iam.service.exception.EntityNotFoundError;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class InvitationServiceImpl implements InvitationService {

    private final GroupService groupService;
    private final EmailSender emailSender;
    private final InvitationRepository invitationRepository;
    private final OwnerProperties ownerProperties;

    @Override
    @Transactional(readOnly = true)
    public SearchResult<Invitation> search(SearchCriteria searchCriteria) {
        Sort sort = buildSort(searchCriteria);
        Pageable pageable = PageRequest.of(searchCriteria.getPage() - 1, searchCriteria.getPageSize(), sort);

        Page<Invitation> page = buildInvitationExample(searchCriteria)
                .map(invitationExample -> invitationRepository.findAll(invitationExample, pageable))
                .orElseGet(() -> invitationRepository.findAll(pageable));

        return new SearchResult<>(searchCriteria, page.getTotalPages(), page.getTotalElements(), page.getContent());
    }

    private Sort buildSort(SearchCriteria searchCriteria) {
        return searchCriteria.getSortBy() != null
                ? Sort.by(searchCriteria.getSortOrder().toDirection(), searchCriteria.getSortBy())
                : Sort.unsorted();
    }

    private Optional<Example<Invitation>> buildInvitationExample(SearchCriteria searchCriteria) {
        return Optional.ofNullable(searchCriteria.getQuery())
                       .map(query -> Example.of(buildInvitationProbe(query), buildMatcher()));
    }

    private Invitation buildInvitationProbe(String query) {
        return Invitation.builder()
                         .email(query)
                         .source(Source.fromString(query))
                         .build();
    }

    private ExampleMatcher buildMatcher() {
        return ExampleMatcher.matchingAny()
                             .withIgnoreCase()
                             .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Invitation> getByToken(String token) {
        return invitationRepository.findByToken(token);
    }

    @Override
    @Transactional
    public List<Invitation> create(Integer invitorId, String invitorUsername, List<Invitation> invitations) {

        User invitor = User.builder()
                           .id(invitorId)
                           .username(invitorUsername)
                           .build();
        return invitations.stream()
                          .peek(invitation -> invitation.setInvitor(invitor))
                          .map(this::createAndSendEmail)
                          .filter(Objects::nonNull)
                          .collect(Collectors.toList());
    }

    private Invitation createAndSendEmail(Invitation invitation) {
        try {
            Integer groupId = invitation.getGroupId();
            Group group = groupService.findInvitableById(groupId)
                                      .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.GROUP_NOT_FOUND_BY_ID, groupId));
            invitation.setGroup(group);

            invitation = createIfEmailAvailable(invitation);
            sendInvitationEmail(invitation);

            return invitation;
        } catch (ApplicationException e) {
            log.error("Invitation can not be sent to email: " + invitation.getEmail(), e);
            return null;
        }
    }

    @Override
    @Transactional
    public Invitation createOwnerInvitation(Integer invitorId, String ownerEmail) {
        String groupName = ownerProperties.getGroup();
        Group group = groupService.getByName(groupName)
                                  .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.GROUP_NOT_FOUND_BY_NAME, groupName));

        Invitation invitation = new Invitation();
        invitation.setEmail(ownerEmail);
        invitation.setSource(Source.INTERNAL);
        invitation.setGroup(group);
        invitation.setInvitor(User.builder().id(invitorId).build());
        return createIfEmailAvailable(invitation);
    }

    private Invitation createIfEmailAvailable(Invitation invitation) {
        String email = invitation.getEmail();
        if (!invitationRepository.isEmailAvailable(email)) {
            throw new BusinessValidationException(BusinessValidationError.USER_EMAIL_ALREADY_EXISTS, email);
        }
        User invitor = invitation.getInvitor();
        Group group = invitation.getGroup();
        invitation = invitationRepository.findByEmail(email)
                                         .orElse(invitation);

        invitation.setInvitor(invitor);
        invitation.setGroup(group);
        invitation.setGroupId(group.getId());
        invitation.setToken(generateToken());

        return invitationRepository.save(invitation);
    }

    private String generateToken() {
        return RandomStringUtils.randomAlphanumeric(100);
    }

    private void sendInvitationEmail(Invitation invitation) {
        if (Source.LDAP == invitation.getSource())
            emailSender.sendUserInvitationLdapEmail(invitation.getToken(), invitation.getEmail());
        else
            emailSender.sendUserInvitationEmail(invitation.getToken(), invitation.getEmail());
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        if (invitationRepository.findById(id).isPresent()) {
            invitationRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException(EntityNotFoundError.INVITATION_NOT_FOUND_BY_ID, id);
        }
    }

}
