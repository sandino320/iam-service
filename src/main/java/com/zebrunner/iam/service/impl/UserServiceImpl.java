package com.zebrunner.iam.service.impl;

import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.entity.Invitation;
import com.zebrunner.iam.domain.entity.Source;
import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.domain.entity.UserStatus;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.domain.search.UserSearchCriteria;
import com.zebrunner.iam.exception.BusinessValidationException;
import com.zebrunner.iam.exception.EntityNotFoundException;
import com.zebrunner.iam.exception.TooManyOperationsException;
import com.zebrunner.iam.messaging.sender.EmailSender;
import com.zebrunner.iam.messaging.sender.UserSavedSender;
import com.zebrunner.iam.persistance.UserRepository;
import com.zebrunner.iam.service.GroupService;
import com.zebrunner.iam.service.InvitationService;
import com.zebrunner.iam.service.LdapService;
import com.zebrunner.iam.service.UserService;
import com.zebrunner.iam.service.config.AdministratorProperties;
import com.zebrunner.iam.service.config.TokenProperties;
import com.zebrunner.iam.service.exception.BusinessValidationError;
import com.zebrunner.iam.service.exception.EntityNotFoundError;
import com.zebrunner.iam.service.exception.TooManyRequestsError;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final LdapService ldapService;
    private final EmailSender emailSender;
    private final GroupService groupService;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final TokenProperties tokenProperties;
    private final UserSavedSender userSavedSender;
    private final InvitationService invitationService;
    private final AdministratorProperties administratorProperties;

    @Override
    @Transactional(readOnly = true)
    public SearchResult<User> search(UserSearchCriteria searchCriteria) {
        return userRepository.findBySearchCriteria(searchCriteria);
    }

    @Override
    public Optional<User> getById(Integer id) {
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> getWithPermissionsById(Integer id) {
        return userRepository.findWithPermissionsById(id);
    }

    @Override
    public Optional<User> getWithPermissionsByUsername(String username) {
        return userRepository.findWithPermissionsByUsername(username);
    }

    @Override
    public Optional<User> getByUsernameOrEmail(String usernameOrEmail) {
        return userRepository.findByUsernameOrEmail(usernameOrEmail);
    }

    @Override
    public Optional<User> getByResetToken(String resetToken) {
        return userRepository.findByResetToken(resetToken);
    }

    @Override
    @Transactional
    public User create(User user) {
        if (userRepository.existsByUsernameOrEmail(user.getUsername(), user.getEmail())) {
            throw new BusinessValidationException(BusinessValidationError.USERNAME_OR_EMAIL_ALREADY_EXISTS);
        }

        user.setId(null);
        user.setStatus(UserStatus.ACTIVE);
        user.setSource(Source.INTERNAL);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        if (user.getGroups().isEmpty() && user.getPermissions().isEmpty()) {
            groupService.getDefault()
                        .map(Set::of)
                        .ifPresent(user::setGroups);
        }

        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
        return user;
    }

    @Override
    @Transactional
    public User create(String invitationToken, User user) {
        String username = user.getUsername();
        if (userRepository.existsByUsername(username)) {
            throw new BusinessValidationException(BusinessValidationError.USERNAME_ALREADY_EXISTS, username);
        }

        Invitation invitation = invitationService.getByToken(invitationToken)
                                                 .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.INVITATION_NOT_FOUND_BY_TOKEN, invitationToken));

        if (invitation.getSource() == Source.LDAP) {
            if (!ldapService.existsUserByUsername(username)) {
                throw new BusinessValidationException(BusinessValidationError.USER_NOT_FOUND_IN_LDAP);
            }
            user.setPassword(null);
        } else {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }

        Integer groupId = invitation.getGroupId();
        Group group = groupService.getById(groupId)
                                  .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.GROUP_NOT_FOUND_BY_ID, groupId));

        user.setGroups(Set.of(group));
        user.setStatus(UserStatus.ACTIVE);
        user.setSource(invitation.getSource());

        invitationService.deleteById(invitation.getId());
        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
        return user;
    }

    @Override
    @Transactional
    public User createAdministrator() {
        return userRepository.findByUsernameOrEmail(administratorProperties.getUsername())
                             .orElseGet(this::createNewAdministrator);
    }

    private User createNewAdministrator() {
        String groupName = administratorProperties.getGroup();
        Group group = groupService.getByName(groupName)
                                  .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.GROUP_NOT_FOUND_BY_NAME, groupName));

        User user = new User();
        user.setUsername(administratorProperties.getUsername());
        user.setPassword(passwordEncoder.encode(administratorProperties.getPassword()));
        user.setGroups(Set.of(group));
        user.setStatus(UserStatus.ACTIVE);
        user.setSource(Source.INTERNAL);

        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
        return user;
    }

    @Override
    @Transactional
    public void patch(User userPatch) {
        Integer userId = userPatch.getId();
        User user = userRepository.findById(userId)
                                  .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.USER_NOT_FOUND_BY_ID, userId));

        if (userPatch.getEmail() != null) {
            if (userRepository.existsByEmailAndIdNot(userPatch.getEmail(), userPatch.getId())) {
                throw new BusinessValidationException(BusinessValidationError.USER_EMAIL_ALREADY_EXISTS);
            }
            user.setEmail(userPatch.getEmail());
        }

        setIfNotNull(userPatch.getStatus(), user::setStatus);
        setIfNotNull(userPatch.getFirstName(), user::setFirstName);
        setIfNotNull(userPatch.getLastName(), user::setLastName);
        setIfNotNull(userPatch.getPhotoUrl(), user::setPhotoUrl);

        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
    }

    private <T> void setIfNotNull(T value, Consumer<T> setter) {
        if (value != null) {
            setter.accept(value);
        }
    }

    @Override
    @Transactional
    public void updatePassword(Integer id, String oldPassword, String newPassword, boolean force) {
        User user = userRepository.findById(id)
                                  .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.USER_NOT_FOUND_BY_ID, id));

        if (!force && (oldPassword == null || !passwordEncoder.matches(oldPassword, user.getPassword()))) {
            throw new BusinessValidationException(BusinessValidationError.WRONG_OLD_PASSWORD);
        }

        user.setPassword(passwordEncoder.encode(newPassword));

        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
    }

    @Override
    @Transactional
    public void updatePassword(String resetToken, String newPassword) {
        User user = userRepository.findByResetToken(resetToken)
                                  .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.USER_NOT_FOUND_BY_RESET_TOKEN));
        user.setResetToken(null);
        user.setPassword(passwordEncoder.encode(newPassword));

        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
    }

    @Override
    public void updateLastLogin(Integer id, Instant lastLogin) {
        userRepository.updateLastLogin(id, lastLogin);
    }

    @Override
    public void addUserToGroup(Integer userId, Integer groupId) {
        User user = userRepository.findWithPermissionsById(userId)
                                  .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.USER_NOT_FOUND_BY_ID, userId));
        if (hasGroup(user, groupId)) {
            throw new BusinessValidationException(BusinessValidationError.USER_HAS_ADDED_TO_GROUP, groupId);
        }

        Group group = groupService.getById(groupId)
                                  .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.GROUP_NOT_FOUND_BY_ID, userId));
        user.getGroups().add(group);

        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
    }

    @Override
    public void removeUserFromGroup(Integer userId, Integer groupId) {
        User user = userRepository.findWithPermissionsById(userId)
                                  .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.USER_NOT_FOUND_BY_ID, userId));
        if (!hasGroup(user, groupId)) {
            throw new BusinessValidationException(BusinessValidationError.USER_HAS_NOT_ADDED_TO_GROUP, groupId);
        }

        user.getGroups().removeIf(group -> group.getId().equals(groupId));

        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
    }

    private boolean hasGroup(User user, Integer groupId) {
        return user.getGroups()
                   .stream()
                   .anyMatch(group -> group.getId().equals(groupId));
    }

    @Override
    @Transactional
    public void sendResetPasswordEmail(String email) {
        User user = userRepository.findByEmail(email)
                                  .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.USER_NOT_FOUND_BY_EMAIL, email));

        if (Source.INTERNAL == user.getSource()) {
            if (isNotValidResetToken(user.getResetTokenValidFrom())) {
                throw new TooManyOperationsException(TooManyRequestsError.OFTEN_PASSWORD_RESETS);
            }

            String resetToken = user.getResetToken() != null
                    ? user.getResetToken()
                    : RandomStringUtils.randomAlphabetic(tokenProperties.getPasswordReset().getChars());
            user.setResetToken(resetToken);
            user.setResetTokenValidFrom(Instant.now());

            user = userRepository.save(user);
            userSavedSender.sendNotification(user);

            emailSender.sendForgotPasswordEmail(resetToken, email);
        } else {
            emailSender.sendForgotPasswordLdapEmail(email);
        }
    }

    private boolean isNotValidResetToken(Instant validFrom) {
        int retryDelay = tokenProperties.getPasswordReset().getRetryInSecs();
        return validFrom != null && !Instant.now().minusSeconds(retryDelay).isAfter(validFrom);
    }
}
