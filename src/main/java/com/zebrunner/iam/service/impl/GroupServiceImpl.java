package com.zebrunner.iam.service.impl;

import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.entity.Permission;
import com.zebrunner.iam.domain.search.SearchCriteria;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.exception.BusinessValidationException;
import com.zebrunner.iam.exception.EntityNotFoundException;
import com.zebrunner.iam.persistance.GroupRepository;
import com.zebrunner.iam.service.GroupService;
import com.zebrunner.iam.service.PermissionService;
import com.zebrunner.iam.service.exception.BusinessValidationError;
import com.zebrunner.iam.service.exception.EntityNotFoundError;
import com.zebrunner.iam.service.util.StreamUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;
    private final PermissionService permissionService;

    @Override
    public SearchResult<Group> search(SearchCriteria searchCriteria) {
        Sort sort = buildSort(searchCriteria);
        Pageable pageable = PageRequest.of(searchCriteria.getPage() - 1, searchCriteria.getPageSize(), sort);
        Optional<Example<Group>> maybeGroupExample = buildGroupExample(searchCriteria);

        Page<Group> page = maybeGroupExample.map(groupExample -> groupRepository.findAll(groupExample, pageable))
                                            .orElseGet(() -> groupRepository.findAll(pageable));

        return new SearchResult<>(searchCriteria, page.getTotalPages(), page.getTotalElements(), page.getContent());
    }

    private Sort buildSort(SearchCriteria searchCriteria) {
        return searchCriteria.getSortBy() != null
                ? Sort.by(searchCriteria.getSortOrder().toDirection(), searchCriteria.getSortBy())
                : Sort.unsorted();
    }

    private Optional<Example<Group>> buildGroupExample(SearchCriteria searchCriteria) {
        if (searchCriteria.getQuery() != null || searchCriteria.isPublic()) {
            Group.GroupBuilder builder = Group.builder();
            if (searchCriteria.getQuery() != null) {
                builder.name(searchCriteria.getQuery());
            }
            if (searchCriteria.isPublic()) {
                builder.invitable(searchCriteria.isPublic());
            }

            // we can use matchingAll() here because we search only by name
            ExampleMatcher matcher = ExampleMatcher.matchingAll()
                                                   .withIgnoreCase()
                                                   .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
            return Optional.of(Example.of(builder.build(), matcher));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Group> getDefault() {
        return groupRepository.findByIsDefaultTrue();
    }

    @Override
    public Optional<Group> getById(Integer id) {
        return groupRepository.findWithPermissionsById(id);
    }

    @Override
    public Optional<Group> getByName(String name) {
        return groupRepository.findByName(name);
    }

    @Override
    public Optional<Group> findInvitableById(Integer id) {
        return groupRepository.findByInvitableTrueAndId(id);
    }

    @Override
    @Transactional
    public Group save(Group group) {
        checkGroupId(group.getId());
        checkGroupName(group);

        Set<String> permissionNames = StreamUtils.mapToSet(group.getPermissions(), Permission::getName);
        Set<Permission> foundPermissions = permissionService.getConciseSetByNames(permissionNames);
        group.setPermissions(foundPermissions);

        if (group.getIsDefault()) {
            groupRepository.setAllNotDefault();
        }
        group = groupRepository.save(group);
        return group;
    }

    private void checkGroupId(Integer id) {
        if (id != null && !groupRepository.existsById(id)) {
            throw new EntityNotFoundException(EntityNotFoundError.GROUP_NOT_FOUND_BY_ID, id);
        }
    }

    private void checkGroupName(Group group) {
        String name = group.getName();
        groupRepository.findByName(name)
                       .map(Group::getId)
                       .filter(Predicate.not(id -> id.equals(group.getId())))
                       .ifPresent(id -> {
                           throw new BusinessValidationException(EntityNotFoundError.GROUP_NOT_FOUND_BY_ID, name);
                       });
    }

    @Override
    @Transactional
    public void setDefault(Integer id, boolean isDefault) {
        Group group = groupRepository.findById(id)
                                     .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.GROUP_NOT_FOUND_BY_ID, id));
        if (group.getIsDefault() != isDefault) {
            groupRepository.setAllNotDefault();
            group.setIsDefault(isDefault);

            groupRepository.save(group);
        }
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        Group group = groupRepository.findById(id)
                                     .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.GROUP_NOT_FOUND_BY_ID, id));

        if (groupRepository.countUsersById(id) != 0) {
            throw new BusinessValidationException(BusinessValidationError.GROUP_HAS_ASSIGNED_USERS);
        }

        groupRepository.delete(group);
    }

}
