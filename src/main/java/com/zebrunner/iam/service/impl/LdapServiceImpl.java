package com.zebrunner.iam.service.impl;

import com.zebrunner.iam.domain.entity.LdapConfiguration;
import com.zebrunner.iam.exception.EntityNotFoundException;
import com.zebrunner.iam.persistance.LdapConfigurationRepository;
import com.zebrunner.iam.service.LdapService;
import com.zebrunner.iam.service.exception.EntityNotFoundError;
import com.zebrunner.iam.service.exception.TooManyRequestsError;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jasypt.util.text.BasicTextEncryptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.ldap.SpringSecurityLdapTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.directory.SearchControls;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LdapServiceImpl implements LdapService {

    private final LdapConfigurationRepository ldapConfigurationRepository;

    @Setter(onMethod = @__(@Value("${service.crypto-salt}")))
    private String cryptoSalt;

    @Override
    public Optional<LdapConfiguration> getConfiguration() {
        return ldapConfigurationRepository.findAll()
                                          .stream()
                                          .map(this::decrypt)
                                          .findFirst();
    }

    private LdapConfiguration decrypt(LdapConfiguration ldapConfiguration) {
        LdapConfiguration clone = ldapConfiguration.clone();

        BasicTextEncryptor encryptor = new BasicTextEncryptor();
        encryptor.setPassword(ldapConfiguration.getPasswordKey() + cryptoSalt);
        clone.setManagerPassword(encryptor.decrypt(ldapConfiguration.getManagerPassword()));

        return clone;
    }

    @Override
    @Transactional
    public void saveConfiguration(LdapConfiguration ldapConfiguration) {
        getConfiguration().map(LdapConfiguration::getId)
                          .ifPresent(ldapConfiguration::setId);

        ldapConfigurationRepository.save(ldapConfiguration);
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public boolean existsUserByUsername(String username) {
        LdapConfiguration ldapConfiguration = getConfiguration()
                .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.LDAP_CONFIGURATION_NOT_FOUND));

        if (ldapConfiguration.isEnabled()) {
            SpringSecurityLdapTemplate ldapTemplate = getLdapTemplate(ldapConfiguration);
            DirContextOperations dirContextOperations = ldapTemplate.searchForSingleEntry(
                    ldapConfiguration.getDn(),
                    ldapConfiguration.getSearchFilter(),
                    new String[]{username}
            );
            return dirContextOperations != null;
        }
        return false;
    }

    @SneakyThrows
    private SpringSecurityLdapTemplate getLdapTemplate(LdapConfiguration ldapConfiguration) {
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        SpringSecurityLdapTemplate template = new SpringSecurityLdapTemplate(getLdapContextSource(ldapConfiguration));
        template.setSearchControls(searchControls);
        template.afterPropertiesSet();
        return template;
    }

    private LdapContextSource getLdapContextSource(LdapConfiguration ldapConfiguration) {
        LdapContextSource ldapContextSource = new LdapContextSource();
        ldapContextSource.setUrl(ldapConfiguration.getUrl());
        ldapContextSource.setUserDn(ldapConfiguration.getManagerUser());
        ldapContextSource.setPassword(ldapConfiguration.getManagerPassword());
        ldapContextSource.afterPropertiesSet();

        return ldapContextSource;
    }

}
