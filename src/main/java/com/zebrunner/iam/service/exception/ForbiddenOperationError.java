package com.zebrunner.iam.service.exception;

import com.zebrunner.iam.exception.ErrorDetail;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ForbiddenOperationError implements ErrorDetail {

    USER_NOT_ACTIVE(1200);

    private final Integer code;

}
