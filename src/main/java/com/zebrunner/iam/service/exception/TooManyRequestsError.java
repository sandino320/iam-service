package com.zebrunner.iam.service.exception;

import com.zebrunner.iam.exception.ErrorDetail;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TooManyRequestsError implements ErrorDetail {

    OFTEN_PASSWORD_RESETS(1500);

    private final Integer code;

}
