package com.zebrunner.iam.service.exception;

import com.zebrunner.iam.exception.ErrorDetail;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum EntityNotFoundError implements ErrorDetail {

    USER_NOT_FOUND_BY_USERNAME(1100),
    USER_NOT_FOUND_BY_ID(1101),
    USER_NOT_FOUND_BY_RESET_TOKEN(1102),
    USER_NOT_FOUND_BY_EMAIL(1103),
    GROUP_NOT_FOUND_BY_ID(1104),
    GROUP_NOT_FOUND_BY_NAME(1105),
    INVITATION_NOT_FOUND_BY_ID(1106),
    INVITATION_NOT_FOUND_BY_TOKEN(1107),
    LDAP_CONFIGURATION_NOT_FOUND(1108),
    PERMISSIONS_NOT_FOUND_BY_NAMES(1109),
    DEFAULT_GROUP_NOT_FOUND(1110);

    private final Integer code;

}
