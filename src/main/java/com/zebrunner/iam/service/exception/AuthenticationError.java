package com.zebrunner.iam.service.exception;

import com.zebrunner.iam.exception.ErrorDetail;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum AuthenticationError implements ErrorDetail {

    INVALID_USER_CREDENTIALS(1400),
    REFRESH_TOKEN_IS_EXPIRED(1401),
    USER_PASSWORD_HAS_CHANGED(1403),
    INVALID_AUTHENTICATION(1404);

    private final Integer code;

}
