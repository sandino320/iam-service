package com.zebrunner.iam.service;

import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.domain.search.UserSearchCriteria;

import java.time.Instant;
import java.util.Optional;

public interface UserService {

    SearchResult<User> search(UserSearchCriteria searchCriteria);

    Optional<User> getById(Integer id);

    Optional<User> getWithPermissionsById(Integer id);

    Optional<User> getWithPermissionsByUsername(String username);

    Optional<User> getByUsernameOrEmail(String usernameOrEmail);

    Optional<User> getByResetToken(String resetToken);

    User create(User user);

    User create(String invitationToken, User user);

    User createAdministrator();

    void patch(User user);

    void updatePassword(Integer id, String oldPassword, String newPassword, boolean force);

    void updatePassword(String resetToken, String newPassword);

    void updateLastLogin(Integer id, Instant lastLogin);

    void addUserToGroup(Integer userId, Integer groupId);

    void removeUserFromGroup(Integer userId, Integer groupId);

    void sendResetPasswordEmail(String email);

}
