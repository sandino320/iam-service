package com.zebrunner.iam.service.config;

import com.zebrunner.iam.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class AdministratorInitializingListener implements ApplicationListener<ApplicationStartedEvent> {

    private final UserService userService;

    @Override
    public void onApplicationEvent(@NonNull ApplicationStartedEvent event) {
        String property = event.getApplicationContext().getEnvironment().getProperty("service.multitenant");
        if (!"true".equalsIgnoreCase(property)) {
            log.info("Creating administrator...");
            try {
                userService.createAdministrator();
                log.info("Administrator was created successfully.");
            } catch (Exception e) {
                log.error("Unable to init default administrator.", e);
            }
        }
    }

}
