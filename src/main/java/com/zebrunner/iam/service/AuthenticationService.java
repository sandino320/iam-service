package com.zebrunner.iam.service;

import com.zebrunner.iam.domain.token.AuthenticationData;

import java.util.Set;

public interface AuthenticationService {

    AuthenticationData authenticate(String username, String password);

    AuthenticationData refresh(String refreshToken);

    String generateServiceAccessToken(Integer userId);

    boolean verify(String authToken, Set<String> permissions);

}
