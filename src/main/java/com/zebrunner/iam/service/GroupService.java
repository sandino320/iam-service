package com.zebrunner.iam.service;

import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.search.SearchCriteria;
import com.zebrunner.iam.domain.search.SearchResult;

import java.util.Optional;

public interface GroupService {

    SearchResult<Group> search(SearchCriteria searchCriteria);

    Optional<Group> getDefault();

    Optional<Group> getById(Integer id);

    Optional<Group> getByName(String name);

    Optional<Group> findInvitableById(Integer id);

    Group save(Group group);

    void setDefault(Integer id, boolean isDefault);

    void deleteById(Integer id);

}
