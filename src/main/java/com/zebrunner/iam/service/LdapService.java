package com.zebrunner.iam.service;

import com.zebrunner.iam.domain.entity.LdapConfiguration;

import java.util.Optional;

public interface LdapService {

    Optional<LdapConfiguration> getConfiguration();

    void saveConfiguration(LdapConfiguration ldapConfiguration);

    boolean existsUserByUsername(String username);

}
