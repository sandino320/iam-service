package com.zebrunner.iam.persistance;

import com.zebrunner.iam.domain.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Integer> {

    List<Permission> findByNameIn(Collection<String> name);

    @Query(value = "select * from permissions where name similar to :searchString", nativeQuery = true)
    List<Permission> findByNameSimilarTo(@Param("searchString") String searchString);

}
