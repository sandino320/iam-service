package com.zebrunner.iam.persistance;

import com.zebrunner.iam.domain.entity.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>, UserSearchRepository {

    @Query("select u from User u where u.username = :usernameOrEmail or u.email = :usernameOrEmail")
    Optional<User> findByUsernameOrEmail(@Param("usernameOrEmail") String usernameOrEmail);

    @EntityGraph("user-with-groups-and-permissions")
    Optional<User> findWithPermissionsById(Integer id);

    @EntityGraph("user-with-groups-and-permissions")
    Optional<User> findWithPermissionsByUsername(String username);

    Optional<User> findByEmail(String email);

    Optional<User> findByResetToken(String resetToken);

    @Modifying
    @Query("update User u set u.lastLogin = :lastLogin where u.id = :id")
    void updateLastLogin(@Param("id") Integer id, @Param("lastLogin") Instant lastLogin);

    boolean existsByUsernameOrEmail(String username, String email);

    boolean existsByUsername(String username);

    boolean existsByEmailAndIdNot(String email, Integer id);

}
