package com.zebrunner.iam.persistance;

import com.zebrunner.iam.domain.entity.Invitation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InvitationRepository extends JpaRepository<Invitation, Integer> {

    Optional<Invitation> findByEmail(String email);

    Optional<Invitation> findByToken(String token);

    @Query("select count(u) = 0 from User u where u.email = :email")
    boolean isEmailAvailable(@Param("email") String email);

}
