package com.zebrunner.iam.messaging.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class RabbitConfig {

    public static final String INTEGRATION_SAVED_EXCHANGE = "integration-saved";
    public static final String INTEGRATION_SAVED_IAM_SERVICE_QUEUE = "integration-saved-iam-service-queue";

    public static final String USER_SAVED_EXCHANGE = "user-saved";
    public static final String USER_SAVED_REPORTING_SERVICE_QUEUE = "user-saved-reporting-service-queue";

    public static final String ZBR_CALLBACKS_QUEUE = "zbrCallbacksQueue";
    public static final String ZBR_CALLBACKS_ROUTING_KEY = "zbr_callbacks";

    public static final String ZFR_CALLBACKS_QUEUE = "zfrCallbacksQueue";
    public static final String ZFR_CALLBACKS_ROUTING_KEY = "zfr_callbacks";

    public static final String MAIL_DATA_EXCHANGE = "mail-data-exchange";
    public static final String MAIL_DATA_ROUTING_KEY = "mail-data";
    public static final String MAIL_DATA_QUEUE = "mail-data-queue";

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public DirectExchange mailDataExchange() {
        return new DirectExchange(MAIL_DATA_EXCHANGE, false, false);
    }

    @Bean
    public Queue mailDataQueue() {
        return new Queue(MAIL_DATA_QUEUE, false);
    }

    @Bean
    public Binding mailDataBinding(DirectExchange mailDataExchange, Queue mailDataQueue) {
        return BindingBuilder.bind(mailDataQueue).to(mailDataExchange).with(MAIL_DATA_ROUTING_KEY);
    }

    //////////////////////////////////////////////////////////
    //                   Integration Saved
    //////////////////////////////////////////////////////////

    @Bean
    public FanoutExchange integrationSavedExchange() {
        return new FanoutExchange(INTEGRATION_SAVED_EXCHANGE);
    }

    @Bean
    public Queue integrationSavedQueue() {
        return new Queue(INTEGRATION_SAVED_IAM_SERVICE_QUEUE);
    }

    @Bean
    public Binding integrationSavedBinding(FanoutExchange integrationSavedExchange, Queue integrationSavedQueue) {
        return BindingBuilder.bind(integrationSavedQueue).to(integrationSavedExchange);
    }

    //////////////////////////////////////////////////////////
    //                      User Saved
    //////////////////////////////////////////////////////////

    @Bean
    public FanoutExchange userSavedExchange() {
        return new FanoutExchange(USER_SAVED_EXCHANGE);
    }

    // it is necessary to explicitly declare reporting-service's queue and binding
    // in order to ensure the correct replication of the tenant administrator user for single tenant deployments
    @Bean
    public Queue userSavedQueue() {
        return new Queue(USER_SAVED_REPORTING_SERVICE_QUEUE);
    }

    @Bean
    public Binding userSavedBinding(FanoutExchange userSavedExchange, Queue userSavedQueue) {
        return BindingBuilder.bind(userSavedQueue).to(userSavedExchange);
    }

    //////////////////////////////////////////////////////////
    //                    Events
    //////////////////////////////////////////////////////////

    @Bean
    public DirectExchange eventsTopicExchange(@Value("${spring.rabbitmq.template.exchange}") String exchangeName) {
        return new DirectExchange(exchangeName, false, true);
    }

    @Bean
    public Queue zbrCallbacksQueue() {
        return new Queue(ZBR_CALLBACKS_QUEUE, false, false, true);
    }

    @Bean
    public Binding zbrCallbacksBinding(DirectExchange eventsTopicExchange, Queue zbrCallbacksQueue) {
        return BindingBuilder.bind(zbrCallbacksQueue).to(eventsTopicExchange).with(ZBR_CALLBACKS_ROUTING_KEY);
    }

    @Bean
    public Queue zfrCallbacksQueue() {
        return new Queue(ZFR_CALLBACKS_QUEUE, false, false, true);
    }

    @Bean
    public Binding zfrCallbacksBinding(DirectExchange eventsTopicExchange, Queue zfrCallbacksQueue) {
        return BindingBuilder.bind(zfrCallbacksQueue).to(eventsTopicExchange).with(ZFR_CALLBACKS_ROUTING_KEY);
    }

}
