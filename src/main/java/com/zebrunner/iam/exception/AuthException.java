package com.zebrunner.iam.exception;

public class AuthException extends ApplicationException {

    public AuthException(ErrorDetail errorDetail) {
        super(errorDetail);
    }

    public AuthException(ErrorDetail errorDetail, String message) {
        super(errorDetail, message);
    }

    public AuthException(Throwable cause, ErrorDetail errorDetail) {
        super(cause, errorDetail);
    }

    public AuthException(Throwable cause, ErrorDetail errorDetail, Object... messageArgs) {
        super(cause, errorDetail, messageArgs);
    }

    public AuthException(ErrorDetail errorDetail, String message, Throwable cause) {
        super(errorDetail, message, cause);
    }

    public AuthException(ErrorDetail errorDetail, String message, Throwable cause, boolean writableStackTrace) {
        super(errorDetail, message, cause, writableStackTrace);
    }

}
