package com.zebrunner.iam.exception;

/**
 * Exception that is thrown to indicate that certain operation is not valid according to business logic of application
 * (e.g. when someone attempts to update resource with a certain status that indicates immutable state at given moment -
 * simply put can not be updated).
 */
public class BusinessValidationException extends ApplicationException {

    public BusinessValidationException(ErrorDetail errorDetail, String message, Object... args) {
        super(errorDetail, message, args);
    }

    public BusinessValidationException(ErrorDetail errorDetail, Object... messageArgs) {
        super(errorDetail, messageArgs);
    }
}
