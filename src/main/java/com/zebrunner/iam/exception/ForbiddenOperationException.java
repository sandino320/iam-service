package com.zebrunner.iam.exception;

public class ForbiddenOperationException extends ApplicationException {

    public ForbiddenOperationException(ErrorDetail errorDetail, String message, Object... args) {
        super(errorDetail, message, args);
    }

    public ForbiddenOperationException(ErrorDetail errorDetail, Object... messageArgs) {
        super(errorDetail, messageArgs);
    }
}
