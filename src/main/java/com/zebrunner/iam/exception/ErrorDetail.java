package com.zebrunner.iam.exception;

public interface ErrorDetail {

    Integer getCode();

}
