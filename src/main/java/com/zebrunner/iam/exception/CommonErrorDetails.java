package com.zebrunner.iam.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CommonErrorDetails implements ErrorDetail {

    INTERNAL_ERROR(1000),
    HTTP_METHOD_NOT_SUPPORTED(1001),
    HTTP_MESSAGE_NOT_READABLE(1002),
    VALIDATION_ERROR(1003),
    HTTP_MEDIA_TYPE_NOT_SUPPORTED(1004),
    ACCESS_IS_DENIED(1005),
    UNAUTHORIZED(1006),
    UNPROCESSABLE_INPUT_STREAM(1007);

    private final Integer code;

}
