package com.zebrunner.iam.exception;

public class TooManyOperationsException extends ApplicationException {

    public TooManyOperationsException(ErrorDetail errorDetail, String message, Object... args) {
        super(errorDetail, message, args);
    }

    public TooManyOperationsException(ErrorDetail errorDetail, Object... messageArgs) {
        super(errorDetail, messageArgs);
    }
}
